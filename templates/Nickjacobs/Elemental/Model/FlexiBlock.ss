<% if $Title && $ShowTitle %>
    <h2 class="flexi-element__title">$Title</h2>
<% end_if %>

<div class="flexi-element__file">
    $File
</div>
