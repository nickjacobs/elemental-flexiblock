# SilverStripe Elemental Flexi Block



This repository contains a "flexi content block" for the [silverstripe-elemental](https://github.com/dnadesign/silverstripe-elemental) module.

## Installation

Install using Composer:

```
composer require nickjacobs/elemental-flexiblock
```

Once complete, run `dev/build` from your browser, or command line via `vendor/bin/sake dev/build`.

## Requirements

* SilverStripe CMS ^4.3
* Elemental ^4.0

## Screen Shots

#### CMS sample of some Flexi Blocks
One image, one PDF

![CMS sample of some File Blocks](./readme-images/example-cms.png)

